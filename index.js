const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

// We import our settings file
const settings = require('./settings');

// We import our polls routes
const pollsRouter = require('./routes/polls');

// We create the express app
const app = express();

// Cors is used for setting up security and access from non localhost devices
const corsOptions = {
  origin: '*',
  allowedHeaders: [
    'origin',
    'x-requested-with',
    'content-type',
    'accept',
    'authorization'
  ]
};

app.use(cors(corsOptions));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// We tell our app that users querying http://myapp.com/polls should be using our pollsRouters routes
app.use('/polls', pollsRouter);

// A simple example showing that express can easily send data. Here, users accessing http://myapp.com will receive "It Works" 
app.get('/', (req, res) => {
  res.send('It Works!');
});

// We actually start the express app
app.listen(settings.port, () => {
  console.log('Listening on port ' + settings.port);
});
