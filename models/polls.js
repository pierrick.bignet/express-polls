const polls = [
  {
    id: 1,
    question: 'Do you like polls ?',
    answers: [{
      id: 124,
      response: 'Oui',
      votes: 14
    }, {
      id: 125,
      response: 'Non',
      votes: 5
    }
    ]
  },
  {
    id: 1,
    question: 'Do you like beers ?',
    answers: [{
      id: 18,
      response: 'Yes',
      votes: 14
    }, {
      id: 19,
      response: 'No',
      votes: 5
    }
    ]
  }
];

module.exports = {
  getAll: () => {
    return new Promise((resolve, reject) => {
      if (typeof (polls) !== 'undefined') {
        resolve(polls)
      } else {
        reject(new Error('No polls'))
      }
    });
  },
  getById: (id) => {
    // You need to code this one!
  }
};
