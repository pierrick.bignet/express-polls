const express = require('express');
const router = express.Router();
const polls = require('../models/polls');

router.get('/', (req, res) => {
  return polls.getAll()
    .then((result) => {
      res.send(result);
    })
    .catch((error) => {
      res.status(500).send(error.message);
    });
});

module.exports = router;
